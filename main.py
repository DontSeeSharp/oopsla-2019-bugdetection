import os
import time
import subprocess
from configparser import ConfigParser
from config import Config
from gpu_selector import GPUSelector

if __name__ == '__main__':
    config = Config.get_default_config()
    os.environ["CUDA_VISIBLE_DEVICES"] = str(GPUSelector(config).pick_gpu_lowest_memory())

    cfg = ConfigParser()
    cfg.read('config.ini')
    path = os.path.join(os.getcwd(), "Code_bug")
    start = time.process_time()
    subprocess.call("python " + os.path.join(path, "localcontext.py"), shell=True)
    local_file = cfg.get('globalcontext', 'test_local_data')
    if os.path.exists(local_file):
        subprocess.call("python " + os.path.join(path, "globalcontext.py"), shell=True)
    else:
        print("Local Context Generation Error")
    time_length = time.process_time() - start
    print("Cost " + str(time_length) + " s to finish the model")