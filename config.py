class Config:

    @staticmethod
    def get_default_config():
        config = Config()
        config.DATA_PATH = ""
        config.PATCH_FILE_PATH = ""
        config.PROCESSED_DATA_PATH = ""
        config.PICK_BEST_GPU = True
        return config

    def __init__(self):
        self.DATA_PATH = None
        self.PATCH_FILE_PATH = None
        self.PROCESSED_DATA_PATH = None
        self.PICK_BEST_GPU = False
